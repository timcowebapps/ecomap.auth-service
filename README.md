```console
netstat -ntlp
sudo netstat -antlp | grep 3264
sudo kill -9 <PID>
```

##### Сборка
```console
mvn clean package
mvn package spring-boot:repackage
```

##### Запуск
```console
cd ./target
java -jar auth-0.0.1-SNAPSHOT.jar
```

```console
docker build --file=Dockerfile --tag=config-server:latest --rm=true .
```

```console
curl -i -X POST \
    http://localhost:3264/api/auth/signin \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
        "username": "admin",
        "password": "123456"
    }'

curl -X GET \
    http://localhost:3264/api/user/v1/find/admin \
    -H 'Authorization: Bearer <TOKEN>'

curl -X GET \
    http://localhost:3264/api/user/v1/gets \
    -H 'Authorization: Bearer <TOKEN>'


curl -i -X POST \
    http://localhost:3264/api/user/v1/register \
    -H 'cache-control: no-cache' \
    -H 'content-type: application/json' \
    -d '{
        "username": "test",
        "email": "test@mail.com",
        "password": "querty"
    }'
```
