#
# Build stage
#
FROM maven:3.3.9-jdk-8-alpine AS build
COPY . ./services/auth
WORKDIR /services/auth

#RUN mvn clean package spring-boot:repackage
RUN mvn clean package

#
# Package stage
#
FROM openjdk:8-jdk-alpine
WORKDIR /services/auth

ARG JAR_FILE=services/auth/target/*.jar
COPY --from=build ${JAR_FILE} security_apiservice.jar

CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "security_apiservice.jar"]
