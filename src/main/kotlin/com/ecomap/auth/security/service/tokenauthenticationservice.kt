package com.ecomap.auth.security.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ecomap.auth.services.UserDetailsServiceImpl;
import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenAuthenticationService {
	companion object {
		private val logger = LoggerFactory.getLogger(TokenAuthenticationService::class.java);
	}

	@Autowired
	private val userDetailsService: UserDetailsServiceImpl? = null

	internal fun getAuthentication(request: HttpServletRequest): Authentication? {
		val token = request.getHeader(SecurityConstants.TOKEN_AUTHORIZATION_HEADER) ?: return null;
		val claims: Claims = Jwts.parser()
			.setSigningKey(SecurityConstants.JWT_SECRET)
			.parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""))
			.getBody();

		val authorities = claims.get(SecurityConstants.AUTHORITIES_KEY).toString()
			.split(",").filterNot(String::isEmpty).map(::SimpleGrantedAuthority/* Ссылка на конструктор */)

		val principal = User(claims.getSubject(), "", authorities);
		return UsernamePasswordAuthenticationToken(principal, /*credentials*/token.replace(SecurityConstants.TOKEN_PREFIX, ""), authorities);
	}
}
