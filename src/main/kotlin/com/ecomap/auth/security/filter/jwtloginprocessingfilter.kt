package com.ecomap.auth.security.filter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ecomap.auth.domain.data.entities.User;
import com.ecomap.auth.security.service.TokenAuthenticationService;
import com.ecomap.auth.security.handler.JwtAuthenticationSuccessHandler;
import com.ecomap.auth.security.handler.JwtAuthenticationFailureHandler;
import java.io.IOException;
import java.util.Collections;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Выполняет проверку предоставленного токена JWT.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @since 18/09/2019
 */
public class JwtLoginProcessingFilter(
	url: String,
	authManager: AuthenticationManager
): AbstractAuthenticationProcessingFilter(AntPathRequestMatcher(url)) {
	companion object {
		private val logger = LoggerFactory.getLogger(JwtLoginProcessingFilter::class.java);
	}

	init {
		authenticationManager = authManager
	}

	/**
	 */
	@Throws(IOException::class, ServletException::class, AuthenticationException::class)
	override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
		var account = ObjectMapper().readValue(request.getInputStream(), User::class.java);

		return getAuthenticationManager().authenticate(
			UsernamePasswordAuthenticationToken(
				account?.username,
				account?.password,
				emptyList<GrantedAuthority>()
			)
		);
	}

	/**
	 */
	@Throws(IOException::class, ServletException::class)
	override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, auth: Authentication) {
		JwtAuthenticationSuccessHandler().onAuthenticationSuccess(request, response, auth);
	}

	/**
	 */
	@Throws(IOException::class, ServletException::class)
	override fun unsuccessfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, failed: AuthenticationException) {
		JwtAuthenticationFailureHandler().onAuthenticationFailure(request, response, failed);
	}
}
