package com.ecomap.auth.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.*;
import java.util.stream.Collectors;

public class TokenHandler() {
	public fun createAccessToken(authentication: Authentication, username: String): String {
		val authorities = authentication.getAuthorities().stream()
			.map(GrantedAuthority::getAuthority)
			.collect(Collectors.joining(","));

		return Jwts.builder()
			.setId(UUID.randomUUID().toString())
			.setSubject(username)
			.claim(SecurityConstants.AUTHORITIES_KEY, authorities)
			.setIssuedAt(Date())
			.setExpiration(Date(System.currentTimeMillis() + SecurityConstants.JWT_EXPIRATION))
			.signWith(SignatureAlgorithm.HS512, SecurityConstants.JWT_SECRET)
			.compact();
	}
}
