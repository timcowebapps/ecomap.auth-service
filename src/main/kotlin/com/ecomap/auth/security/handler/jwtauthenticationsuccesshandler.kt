package com.ecomap.auth.security.handler;

import org.springframework.stereotype.Component;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ecomap.auth.security.TokenHandler;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JwtAuthenticationSuccessHandler: AuthenticationSuccessHandler {
	@Throws(IOException::class, ServletException::class)
	override fun onAuthenticationSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {
		var userContext: String = authentication.getName();
		var accessToken: String = TokenHandler().createAccessToken(authentication, userContext);

		var tokenHashMap = HashMap<String, String>();
		tokenHashMap.put("token", accessToken);

		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.addHeader(SecurityConstants.TOKEN_AUTHORIZATION_HEADER, SecurityConstants.TOKEN_PREFIX + accessToken);
		ObjectMapper().writeValue(response.getWriter(), tokenHashMap);
	}
}
