package com.ecomap.auth.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.Serializable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Точка входа аутентификации, которая просто отклоняет доступ.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @since 18/09/2019
 */
@Component
public class JwtAuthenticationEntryPoint: AuthenticationEntryPoint {
	companion object {
		private val logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint::class.java);
	}

	@Throws(IOException::class, ServletException::class)
	override fun commence(request: HttpServletRequest, response: HttpServletResponse, exception: AuthenticationException) {
		logger.error("Unauthorized error. Message - {}", exception.message);
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Access Denied");
	}
}
