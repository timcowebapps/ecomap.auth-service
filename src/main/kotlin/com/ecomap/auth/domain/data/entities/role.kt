package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Сущность роли.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {String} name - Имя роли.
 * @property {String} description - Краткое описание (Optional).
 */
@Entity
@Table(name = "t_roles",
	indexes = arrayOf(Index(name = "role_pkey", unique = true, columnList = "id")))
data class Role(
	@ApiModelProperty(value = "Имя роли")
	@Column(name = "role_name", nullable = false, unique = true)
	public var name: String = "",

	@Column(name = "description")
	public var description: String? = null,

	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "t_roles_permissions",
		joinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")],
		inverseJoinColumns = [JoinColumn(name = "permission_id", referencedColumnName = "permission_pk")])
	val permissions: Set<Permission> = HashSet()
): BaseEntity() {
	// Empty
}
