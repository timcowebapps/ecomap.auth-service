package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Модель потенциального виновника.
 *
 * @property {String} name - Имя виновника.
 */
data class Culprit(
	var name: String
);

/**
 * Модель предприятия.
 *
 * @property {String} name - Название предприятия.
 */
@Embeddable
data class Enterprise(
	var name: String
);
