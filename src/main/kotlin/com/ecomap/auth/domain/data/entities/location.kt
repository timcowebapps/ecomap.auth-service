package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Модель локации
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {Double} latitude - Широта.
 * @property {Double} longitude - Долгота.
 */
@Embeddable
data class Location(
	@Column(name = "lat")
	public var latitude: Double,

	@Column(name = "lng")
	public var longitude: Double
);
