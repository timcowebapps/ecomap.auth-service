package com.ecomap.auth.domain.data.entities;

import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;
import javax.persistence.Id;

/**
 * Сущность пользователя.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {String} username - Псевдоним пользователя.
 * @property {String} email - Адрес электронной почты.
 * @property {String} password - Пароль.
 * @property {Boolean} verified - Возвращается **1**, если страница пользователя верифицирована, **0** — если не верифицирована.
 * @property {UserProfile} profile - Профиль пользователя (Optional).
 * @property {MutableList<Role>} roles - Коллекция ролей пользователя.
 */
@Entity
@Table(name = "t_users",
	indexes = arrayOf(Index(name = "user_pkey", unique = true, columnList = "id")))
data class User(
	@Column(name = "user_name", nullable = false, unique = true, updatable = false)
	var username: String = "",

	@Column(name = "email", nullable = false, unique = true)
	var email: String = "",

	@Column(name = "password", nullable = false)
	var password: String = "",

	@Column(name = "verified", nullable = false)
	var verified: Boolean = false,

	@OneToOne(cascade = arrayOf(CascadeType.ALL))
	var profile: UserProfile? = null,

	@ManyToMany(cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER)
	@JoinTable(name = "t_users_roles",
		joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
		inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")])
	var roles: MutableList<Role> = mutableListOf()
): BaseEntity() {
	override fun toString(): String = "User[$id|@$username]";
}
