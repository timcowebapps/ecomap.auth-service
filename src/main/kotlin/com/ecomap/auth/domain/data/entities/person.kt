package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;

/**
 * Сущность профиля пользователя.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {Long} id - Уникальный идентификатор профиля.
 * @property {PersonFullname} fullname - Имя и Фамилия пользователя.
 * @property {Date} birthday - Дата рождения.
 * @property {String} mobilePhone - Номер мобильного телефона.
 */
@MappedSuperclass
open class Person(
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "profile_pk", nullable = false)
	public var id: Long = -1,

	@Embedded
	@AttributeOverrides(
		AttributeOverride(name = "firstName", column = Column(name = "first_name")),
		AttributeOverride(name = "lastName", column = Column(name = "last_name")))
	public val fullname: PersonFullname? = null,

	@Column(name = "birthday")
	public var birthday: Date? = null
): Serializable {
	// Empty
}
