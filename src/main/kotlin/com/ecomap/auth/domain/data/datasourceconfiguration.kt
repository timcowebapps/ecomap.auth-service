package com.ecomap.auth.domain.data;

import org.h2.jdbcx.JdbcDataSource;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
	entityManagerFactoryRef = "entityManagerFactory",
	basePackages = arrayOf("com.ecomap.auth.domain.access.repositories")
)
open class DataSourceConfiguration {

	@Value("classpath:schema.sql")
	private lateinit var H2_SCHEMA_SCRIPT: Resource;

	@Value("classpath:data.sql")
	private lateinit var H2_DATA_SCRIPT: Resource;

	@Value("classpath:drop.sql")
	private lateinit var H2_DROP_SCRIPT: Resource;

	@Autowired
	private lateinit var environment: Environment;

	@Autowired
	@Bean
	fun dataSourceInitializer(dataSource: DataSource): DataSourceInitializer {
		var initializer = DataSourceInitializer();
		initializer.setDataSource(dataSource);
		initializer.setDatabasePopulator(databasePopulator());
		initializer.setDatabaseCleaner(databaseCleaner());
		return initializer;
	}

	fun databasePopulator(): DatabasePopulator {
		var populator = ResourceDatabasePopulator();
		populator.addScript(H2_SCHEMA_SCRIPT);
		populator.addScript(H2_DATA_SCRIPT);
		return populator;
	}

	fun databaseCleaner(): DatabasePopulator {
		var populator = ResourceDatabasePopulator();
		populator.addScript(H2_DROP_SCRIPT);
		return populator;
	}

	@Bean
	open fun dataSource(): DataSource {
		var source = JdbcDataSource();
		source.setURL(String.format(environment.getProperty("spring.datasource.url")!!, System.getProperty("user.dir")));
		source.setUser(environment.getProperty("spring.datasource.username"));
		source.setPassword(environment.getProperty("spring.datasource.password"));
		return source;
	}

	@Bean
	fun entityManagerFactory(): LocalContainerEntityManagerFactoryBean {
		var props = Properties();
		props.put("hibernate.hbm2ddl.auto", environment.getProperty("spring.jpa.hibernate.ddl-auto"));

		var adapter = HibernateJpaVendorAdapter();
		adapter.setGenerateDdl(true);
		adapter.setShowSql(true);

		var factory = LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(adapter);
		factory.setPackagesToScan("com.ecomap.auth.domain.data.entities");
		factory.setDataSource(dataSource());

		factory.setJpaProperties(props);
		factory.setLoadTimeWeaver(InstrumentationLoadTimeWeaver());

		return factory;
	}

	@Bean
	fun transactionManager(entityManagerFactory: EntityManagerFactory): PlatformTransactionManager {
		var transactionManager = JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}
}
