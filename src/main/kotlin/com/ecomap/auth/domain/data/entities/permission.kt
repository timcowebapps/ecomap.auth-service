package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ecomap.auth.domain.data.entities.Role;
import javax.persistence.*;

@Entity
@Table(name = "t_permissions")
data class Permission(
	@ApiModelProperty(value = "Уникальный идентификатор")
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "permission_pk")
	val id: Long = 0,

	@ApiModelProperty(value = "Имя разрешения")
	@Column(nullable = false, unique = true)
	val name: String = "",

	@JsonIgnore
	@ManyToMany(mappedBy = "permissions", fetch = FetchType.LAZY)
	val roles: Set<Role> = HashSet()
)
