package com.ecomap.auth.domain.data;

interface IDtoMapper<in E, out D> {
	fun mapFromEntities(entities: Collection<E>): Collection<D> {
		return entities.map { fromEntity(it) }
	}

	fun fromEntity(entity: E): D
}
