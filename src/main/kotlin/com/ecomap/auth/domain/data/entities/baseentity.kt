package com.ecomap.auth.domain.data.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.MappedSuperclass;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;

/**
 * Базовая модель сущности.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {Long} id - Уникальный идентификатор.
 * @property {LocalDateTime} dateCreated - Дата создания записи.
 * @property {LocalDateTime} lastUpdated - Дата обновения записи.
 */
@MappedSuperclass
open class BaseEntity(
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public var id: Long = -1,

	@Column(value = "date_created", updatable = false,
		columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public var dateCreated: LocalDateTime = LocalDateTime.now(),

	@Column(value = "last_updated",
		columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	public var lastUpdated: LocalDateTime = LocalDateTime.now()
): Serializable;
