package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;

/**
 * Сущность профиля пользователя.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {Long} id - Уникальный идентификатор профиля.
 * @property {String} firstName - Имя пользователя.
 * @property {String} lastName - Фамилия пользователя.
 * @property {String} middleName - Отчество пользователя.
 * @property {Date} birthday - Дата рождения.
 * @property {String} mobilePhone - Номер мобильного телефона (Optional).
 * @property {Location} location - Координаты местопложения (Optional).
 */
@Entity
@Table(name = "t_user_profiles")
data class UserProfile(
	@Column(name = "mobile_phone")
	public var mobilePhone: String? = null,

	@Embedded
	public val location: Location? = null
): Person() {
	@JsonIgnore
	@OneToOne(mappedBy = "profile", fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id", nullable = false)
	public lateinit var user: User;
}
