package com.ecomap.auth.domain.data.entities;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Сущность почтового адреса.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {String} country - Страна (Optional).
 * @property {String} state - Штат (Optional).
 * @property {String} city - Название города (Optional).
 * @property {String} street - Название улицы (Optional).
 * @property {String} zipCode - Почтовый индекс (Optional).
 */
@Entity
@Table(name = "t_adresses",
	indexes = arrayOf(Index(name = "address_pkey", unique = true, columnList = "id")))
data class PostalAddress(
	@ApiModelProperty(value = "Страна")
	@Column(name = "country")
	public var country: String? = null,

	@ApiModelProperty(value = "Штат")
	@Column(name = "state")
	public var state: String? = null,

	@ApiModelProperty(value = "Название города")
	@Column(name = "city")
	public var city: String? = null,

	@ApiModelProperty(value = "Название улицы")
	@Column(name = "street")
	public var street: String? = null,

	@ApiModelProperty(value = "Почтовый индекс")
	@Column(name = "zip_сode")
	public var zipCode: String? = null
): BaseEntity() {
	// Empty
}
