package com.ecomap.auth.domain.access.repositories;

import com.ecomap.auth.domain.data.entities.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import java.util.*;

@RepositoryRestResource
@Transactional(propagation = Propagation.MANDATORY)
interface IProfileRepository : JpaRepository<UserProfile, Long> {
	// Empty
}
