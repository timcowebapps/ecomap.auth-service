package com.ecomap.auth.domain.access.repositories;

import com.ecomap.auth.domain.data.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import java.util.*;

@RepositoryRestResource
@Transactional(propagation = Propagation.MANDATORY)
interface IUserRepository : JpaRepository<User, Long> {
	fun findOneByUsernameOrEmail(username: String, email: String): Optional<User>;
	fun findOneByUsername(username: String): Optional<User>;
	fun findOneByEmail(email: String): Optional<User>;

	fun existsByUsername(username: String): Boolean;
	fun existsByEmail(email: String): Boolean;
}
