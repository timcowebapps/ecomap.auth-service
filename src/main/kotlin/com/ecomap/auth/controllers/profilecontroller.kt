package com.ecomap.auth.controllers;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ecomap.auth.exceptions.NotFoundException;
import com.ecomap.auth.models.ProfileCreateInfo;
import com.ecomap.auth.models.UserRegisterCommandDto;
import com.ecomap.auth.domain.data.entities.UserProfile;
import com.ecomap.auth.domain.access.repositories.IProfileRepository;
import java.util.*;
import javax.validation.Valid;

@Api(description = "Контроллер профиля")
@RestController
@RequestMapping("/api/profile")
class ProfileController (
	private val repo: IProfileRepository
) {
	companion object {
		private val logger = LoggerFactory.getLogger(ProfileController::class.java);
	}

	@ApiOperation(value = "Создает профиль пользователя")
	@RequestMapping(method = arrayOf(RequestMethod.POST), value = arrayOf("/v1/create"))
	@ResponseStatus(HttpStatus.CREATED)
	fun createProfile(@Valid @RequestBody profile: ProfileCreateInfo) {
		var newprofile = UserProfile();
		newprofile.fullname!!.firstName = profile.firstName;
		newprofile.fullname!!.lastName = profile.lastName;
		repo.save(newprofile);
	}
}
