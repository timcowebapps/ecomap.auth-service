package com.ecomap.auth.controllers;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ecomap.auth.exceptions.NotFoundException;
import com.ecomap.auth.domain.data.entities.User;
import com.ecomap.auth.domain.access.repositories.IUserRepository;
import com.ecomap.auth.models.UserRegisterCommandDto;
import com.ecomap.auth.services.IUserQueryService;
import java.util.*;
import javax.validation.Valid;

@Api(description = "Контроллер для управления учетной записью текущего пользователя")
@RestController
@RequestMapping("/api/user")
class UserQueryRestController (
	private val repo: IUserRepository,
	private val service: IUserQueryService
) {
	companion object {
		private val logger = LoggerFactory.getLogger(UserQueryRestController::class.java);
	}

	@ApiOperation(value = "Возвращает пользователя по имени")
	@RequestMapping(value = arrayOf("/v1/find/{username}"), method = arrayOf(RequestMethod.GET))
	fun findByUsername(
			@ApiParam(value = "Имя пользователя", required = true)
			@PathVariable(value = "username")
			username: String): ResponseEntity<Optional<User>> {
		val result = service.findByUsername(username)
			//?: throw NotFoundException("Could not find user with username '$username'");
		return if (result != null) {
			ResponseEntity.ok(result);
		} else {
			ResponseEntity.notFound().build();
		}
	}

	/**
	 * Возвращает подробную информацию о пользователях.
	 */
	@ApiOperation(value = "Возвращает всех пользователей")
	@RequestMapping(value = arrayOf("/v1/gets"), method = arrayOf(RequestMethod.GET))
	fun getUsers(): List<User> = repo.findAll();
}
