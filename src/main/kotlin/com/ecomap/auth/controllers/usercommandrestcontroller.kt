package com.ecomap.auth.controllers;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ecomap.auth.exceptions.NotFoundException;
import com.ecomap.auth.domain.data.entities.User;
import com.ecomap.auth.domain.access.repositories.IUserRepository;
import com.ecomap.auth.models.UserRegisterCommandDto;
import com.ecomap.auth.models.UserUpdatePasswordCommandDto;
import com.ecomap.auth.services.IUserCommandService;
import java.util.*;
import javax.validation.Valid;

@Api(description = "Контроллер для управления учетной записью текущего пользователя")
@RestController
@RequestMapping("/api/user")
class UserCommandRestController (
	private val repo: IUserRepository,
	private val service: IUserCommandService
) {
	companion object {
		private val logger = LoggerFactory.getLogger(UserCommandRestController::class.java);
	}

	/**
	 * POST  /api/user/v1/register : Регистрация пользователя.
	 */
	@ApiOperation(value = "Регистрирует нового пользователя")
	@RequestMapping(method = arrayOf(RequestMethod.POST), value = arrayOf("/v1/register"))
	@ResponseStatus(HttpStatus.CREATED)
	fun register(@Valid @RequestBody registerDto: UserRegisterCommandDto) {
		if (logger.isDebugEnabled())
			logger.debug("Save data of a member");

		val newuser = service.createUser(registerDto);
	}

	/**
	 * GET  /api/user/v1/activate : Активация зарегистрированного пользователя.
	 *
	 * @param key Ключ активации.
	 * @throws RuntimeException 500 (Internal Server Error) Если пользователь не может быть активирован.
	 */
	@ApiOperation(value = "Активирует зарегистрированного пользователя")
	@RequestMapping(method = arrayOf(RequestMethod.GET), value = arrayOf("/v1/activate"))
	fun activate(@RequestParam(value = "key") key: String) {
		//
	}

	/**
	 * GET  /api/user/v1/authenticate : Проверьте, аутентифицирован ли пользователь, и верните его логин.
	 */
	// @ApiOperation(value = "Активация зарегистрированного пользователя")
	// @RequestMapping(method = [RequestMethod.GET], value = ["/v1/authenticate"])
	// fun authenticate(request: HttpServletRequest) {
	// 	logger.debug("REST request to check if the current user is authenticated");
	// }

	/**
	 * POST  /api/user/v1/reset-password : Изменяет пароль текущего пользователя.
	 */
	@ApiOperation(value = "Изменяет пароль текущего пользователя")
	@RequestMapping(method = arrayOf(RequestMethod.POST), value = arrayOf("/v1/change-password"))
	fun changePassword(@Valid @RequestBody user: UserUpdatePasswordCommandDto) {
		//
	}
}
