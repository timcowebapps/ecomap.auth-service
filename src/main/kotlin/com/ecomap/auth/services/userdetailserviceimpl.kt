package com.ecomap.auth.services;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ecomap.auth.domain.data.entities.Role;
import com.ecomap.auth.domain.access.repositories.IUserRepository;

/**
 * Для определения ролей пользователей.
 */
@Service("userDetailsService")
class UserDetailsServiceImpl(
	private val repo: IUserRepository
) : UserDetailsService {
	companion object {
		private val logger = LoggerFactory.getLogger(UserDetailsServiceImpl::class.java);
	}

	@Transactional
	@Throws(UsernameNotFoundException::class)
	override fun loadUserByUsername(username: String): UserDetails {
		val user = repo.findOneByUsername(username).get()?: throw UsernameNotFoundException("User '$username' not found")

		return org.springframework.security.core.userdetails.User
			.withUsername(username)
			.password(user.password)
			.authorities(getGrantedAuthorities(user.roles))
			.accountExpired(false)
			.accountLocked(false)
			.credentialsExpired(false)
			.disabled(false)
			.build()
	}

	private fun getGrantedAuthorities(roles: Collection<Role>): MutableCollection<out GrantedAuthority> {
		val permissions = HashSet<String>();
		roles.map { role ->
			permissions.add(role.name!!)
			role.permissions.mapTo(permissions) { permission -> permission.name }
		}

		val authorities = ArrayList<GrantedAuthority>()
		permissions.mapTo(authorities) { permission ->
			logger.info("Granted permission is $permission")
			SimpleGrantedAuthority(permission)
		}

		return authorities;
	}
}
