package com.ecomap.auth.services;

import java.util.*;
import com.ecomap.auth.domain.data.entities.User;
import com.ecomap.auth.models.UserRegisterCommandDto;

interface IUserCommandService {
	fun createUser(register: UserRegisterCommandDto): Optional<User>
	fun deleteUser(username: String)
}
