package com.ecomap.auth.services;

import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
import com.ecomap.auth.exceptions.NotFoundException;
import com.ecomap.auth.domain.data.entities.User;
import com.ecomap.auth.domain.data.entities.UserProfile;
import com.ecomap.auth.domain.access.repositories.IUserRepository;
import com.ecomap.auth.models.UserRegisterCommandDto;

@Service
class UserCommandServiceImpl(
	private val repo: IUserRepository,
	private val encoder: PasswordEncoder
): IUserCommandService {
	companion object {
		private val logger = LoggerFactory.getLogger(UserCommandServiceImpl::class.java);
	}

	@Transactional
	override fun createUser(register: UserRegisterCommandDto): Optional<User> {
		// Создаем пользователя
		var newuser = UserRegisterCommandDto.toUser(register);
		if (repo.findOneByUsername(newuser.username!!).isPresent)
			return Optional.empty();

		// Сохраняем изменения в бд
		return Optional.of(repo.save(newuser.copy(password = encoder.encode(newuser.password))));
	}

	@Transactional
	override fun deleteUser(username: String) {
		// Empty
	}
}
