package com.ecomap.auth.services;

import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
import com.ecomap.auth.exceptions.NotFoundException;
import com.ecomap.auth.domain.data.entities.User;
import com.ecomap.auth.domain.data.entities.UserProfile;
import com.ecomap.auth.domain.access.repositories.IUserRepository;

@Service
class UserQueryServiceImpl(
	private val repo: IUserRepository
): IUserQueryService {
	companion object {
		private val logger = LoggerFactory.getLogger(UserQueryServiceImpl::class.java);
	}

	@Transactional
	override fun findByUsername(username: String) = repo.findOneByUsername(username);
}
