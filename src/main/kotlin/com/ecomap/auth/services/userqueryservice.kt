package com.ecomap.auth.services;

import com.ecomap.auth.domain.data.entities.User;
import java.util.Optional;

interface IUserQueryService {
	fun findByUsername(username: String): Optional<User>
}
