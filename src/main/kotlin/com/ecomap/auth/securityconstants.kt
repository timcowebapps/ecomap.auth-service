object SecurityConstants {
	const val JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y\$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";
	const val JWT_EXPIRATION = 15 * 60 * 1000;

	const val TOKEN_AUTHORIZATION_HEADER = "Authorization";
	const val TOKEN_PREFIX = "Bearer ";
	// val TOKEN_TYPE = "JWT"
	// val TOKEN_ISSUER = "secure-api"
	// val TOKEN_AUDIENCE = "secure-app"

	const val AUTHORITIES_KEY = "auth";

	const val SIGN_UP_URL = "/api/auth/signup";
	const val SIGN_IN_URL = "/api/auth/signin";
	const val VERIFY_URL = "/api/auth/verifyemail";
	const val SEND_RESET_URL = "/api/auth/resetcode";
	const val RESET_PASSWORD_URL = "/api/auth/resetpassword";
	const val PROFILE_URL = "/users/profile";
}
