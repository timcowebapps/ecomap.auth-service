package com.ecomap.auth.configuration;

import org.springframework.http.HttpMethod;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import com.ecomap.auth.security.JwtAuthenticationEntryPoint;
import com.ecomap.auth.security.filter.JwtLoginProcessingFilter;
import com.ecomap.auth.security.JWTAuthenticationFilter;
import com.ecomap.auth.domain.access.repositories.IUserRepository;
import com.ecomap.auth.services.UserDetailsServiceImpl;

@EnableWebSecurity
open class SecurityConfiguration: WebSecurityConfigurerAdapter() {
	@Autowired val userDetailsService: UserDetailsService? = null;

	@Throws(Exception::class)
	override fun configure(http: HttpSecurity) {
		http.csrf().disable()
			.exceptionHandling()
			.authenticationEntryPoint(JwtAuthenticationEntryPoint())
		.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
			.anonymous()
		.and()
			.authorizeRequests()
			.antMatchers(SecurityConstants.SIGN_IN_URL).permitAll()
			.antMatchers("/api/user/v1/register").permitAll()
			// .antMatchers("/api/account/v1/activate").permitAll()
			// .antMatchers("/api/account/v1/authenticate").permitAll()
			// .antMatchers("/api/account/v1/change-password").permitAll()
			.antMatchers("/api/**").authenticated()
			.antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
			.anyRequest().authenticated()
		.and()
			.addFilterBefore(JwtLoginProcessingFilter(SecurityConstants.SIGN_IN_URL, authenticationManager()), UsernamePasswordAuthenticationFilter::class.java)
			.addFilterBefore(JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
			.headers().cacheControl();

		http.cors().configurationSource {
			CorsConfiguration().applyPermitDefaultValues()
		}
	}

	@Throws(Exception::class)
	override fun configure(web: WebSecurity) {
		web.ignoring().antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**");
	}

	@Bean
	open fun passwordEncoder(): PasswordEncoder {
		return BCryptPasswordEncoder();
	}

	@Throws(Exception::class)
	override fun configure(authenticationManagerBuilder: AuthenticationManagerBuilder) {
		authenticationManagerBuilder
			.userDetailsService(userDetailsService)
			.passwordEncoder(passwordEncoder());
	}

	@Bean
	@Throws(Exception::class)
	override fun authenticationManagerBean(): AuthenticationManager {
		return super.authenticationManagerBean();
	}
}
