package com.ecomap.auth.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.Collections;

@Configuration
@EnableSwagger2
class SwaggerConfiguration {
	@Bean
	open fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
		.select()
		.apis(RequestHandlerSelectors.basePackage("com.ecomap.auth.controllers"))
		.paths(PathSelectors.any())
		.build()
		.apiInfo(apiInfo())

	fun apiInfo(): ApiInfo? {
		return ApiInfoBuilder()
			.title("Ecomap-Auth API")
			.description("API for Ecomap applications")
			.version("0.1.0")
			.license("Apache 2.0")
			.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
			.termsOfServiceUrl("htpp://swagger.io/terms")
			.contact(Contact("Victor Timoshin", "", "victor-timoshin@hotmail.com"))
			.build()
	}
}
