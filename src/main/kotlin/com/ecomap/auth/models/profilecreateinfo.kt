package com.ecomap.auth.models;

import io.swagger.annotations.ApiModelProperty;
import com.ecomap.auth.domain.data.IDtoMapper;
import com.ecomap.auth.domain.data.entities.UserProfile;
import java.util.Date;

/**
 * Модель создания профиля пользователя.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {String} firstName - Имя пользователя.
 * @property {String} lastName - Фамилия пользователя.
 * @property {String} middleName - Отчество пользователя.
 * @property {Date} birthday - Дата рождения.
 * @property {String} mobilePhone - Номер мобильного телефона.
 */
data class ProfileCreateInfo(
	@ApiModelProperty(value = "Устанавливает имя пользователя.")
	public var firstName: String? = null,

	@ApiModelProperty(value = "Устанавливает фамилию пользователя.")
	public var lastName: String? = null,

	@ApiModelProperty(value = "Устанавливает дату рождения пользователя.")
	public var birthday: Date? = null,

	@ApiModelProperty(value = "Устанавливает номер мобильного телефона.")
	public var mobilePhone: String? = null
) {
	companion object: IDtoMapper<UserProfile, ProfileCreateInfo> {
		override fun fromEntity(entity: UserProfile) =
			ProfileCreateInfo(
				firstName = entity.fullname!!.firstName,
				lastName = entity.fullname!!.lastName,
				birthday = entity.birthday,
				mobilePhone = entity.mobilePhone
			);
	}
}
