package com.ecomap.auth.models;

// import org.hibernate.validator.constraints.Email;
// import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ecomap.auth.domain.data.IDtoMapper;
import com.ecomap.auth.domain.data.entities.User;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserQueryDto(
	//@ApiModelProperty(value = "Дата создания", required = true, example = "2019-09-28T11:20:05.322+0000")
	@ApiModelProperty(value = "Получает или устанавливает уникальный идентификатор пользователя.")
	@JsonProperty("id")
	var id: Long = -1,

	@ApiModelProperty(value = "Получает или устанавливает псевдоним пользователя.")
	@JsonProperty("username")
	var username: String = ""
) {
	companion object: IDtoMapper<User, UserQueryDto> {
		override fun fromEntity(entity: User) =
			UserQueryDto(
				id = entity.id,
				username = entity.username
			);
	}
}
