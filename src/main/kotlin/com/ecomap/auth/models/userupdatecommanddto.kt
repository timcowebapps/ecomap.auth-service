package com.ecomap.auth.models;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ecomap.auth.domain.data.entities.User;

/**
 * Модель вставки (регистрация) пользователя.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {String} username - Псевдоним пользователя.
 * @property {String} email - Адрес электронной почты.
 * @property {String} password - Пароль.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserUpdateCommandDto(
	@ApiModelProperty(value = "Адрес электронной почты.")
	@JsonProperty("email")
	var email: String = ""
) {
	// Empty
}
