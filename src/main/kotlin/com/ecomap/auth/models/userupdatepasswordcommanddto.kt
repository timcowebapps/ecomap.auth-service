package com.ecomap.auth.models;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ecomap.auth.domain.data.entities.User;

/**
 * Модель вставки (регистрация) пользователя.
 *
 * @author Victor Timoshin <victor-timoshin@hotmail.com>
 * @property {String} oldPassword - Текущий пароль.
 * @property {String} password - Новый пароль.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserUpdatePasswordCommandDto(
	@ApiModelProperty(value = "Текущий пароль.")
	@JsonProperty("old_password")
	var oldPassword: String = "",

	@ApiModelProperty(value = "Новый пароль.")
	@JsonProperty("password")
	var password: String = ""
) {
	// Empty
}
