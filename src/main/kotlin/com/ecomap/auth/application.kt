package com.ecomap.auth;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
open class Application

fun main(args: Array<String>) {
	SpringApplication.run(Application::class.java, *args)
}
