INSERT INTO t_roles(role_name) VALUES('ADMIN');
INSERT INTO t_user_profiles(first_name, last_name, birthday, mobile_phone) VALUES('vic', 'tim', '20011210', '44444444');
INSERT INTO t_users(user_name, email, password, verified, profile_profile_pk) VALUES('admin', 'victor-timoshin@hotmail.com', '$2a$10$GYCkBzp2NlpGS/qjp5f6NOWHeF56ENAlHNuSssSJpE1MMYJevHBWO', 1, 1);
INSERT INTO t_users_roles(user_id, role_id) VALUES(1, 1);

-- INSERT INTO t_permissions(name) VALUES('WRITE');
-- INSERT INTO t_roles_permissions(role_id, permission_id) VALUES(1, 1);
