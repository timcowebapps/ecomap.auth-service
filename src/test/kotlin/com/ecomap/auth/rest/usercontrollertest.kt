package com.ecomap.auth.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Assert.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.ecomap.auth.domain.access.repositories.IUserRepository;
import com.ecomap.auth.models.UserRegisterCommandDto;
import com.ecomap.auth.services.IUserCommandService;
import com.ecomap.auth.controllers.UserCommandRestController;
import java.nio.charset.StandardCharsets;
//import java.util.*;

@RunWith(SpringRunner::class)
@SpringBootTest
class UserControllerTests {
	companion object {
		var DEFAULT_LOGIN: String = "demo";
		var UPDATED_LOGIN: String = "test";

		var DEFAULT_PASSWORD: String = "passdemo";
		var UPDATED_PASSWORD: String = "passtest";

		var DEFAULT_EMAIL: String = "demo@localhost";
		var UPDATED_EMAIL: String = "test@localhost";

		var DEFAULT_FIRSTNAME: String = "demo";
		var UPDATED_FIRSTNAME: String = "demofn";

		var DEFAULT_LASTNAME: String = "test";
		var UPDATED_LASTNAME: String = "testln";

		var APPLICATION_JSON_UTF8: MediaType = MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);
	}

	@Autowired
	private lateinit var context: WebApplicationContext;

	@Autowired
	private lateinit var userRepository: IUserRepository;

	@Autowired
	private lateinit var userService: IUserCommandService;

	private lateinit var mockMvc: MockMvc;

	@Before
	public fun setup() {
		//MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(UserCommandRestController(userRepository, userService)).build();
	}

	@Test
	@Transactional
	public fun createUser() {
		var databaseSizeBeforeCreate = userRepository.findAll().size;

		var mapper = ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.registerModule(JavaTimeModule());

		var registerDto = UserRegisterCommandDto(
			DEFAULT_LOGIN,
			DEFAULT_EMAIL,
			DEFAULT_PASSWORD
		);

		mockMvc.perform(post("/api/user/v1/register")
			.contentType(APPLICATION_JSON_UTF8)
			.content(mapper.writeValueAsBytes(registerDto)))
			//.andExpect(status().isBadRequest());
			.andExpect(status().isCreated());
	}
}
